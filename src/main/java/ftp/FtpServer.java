package ftp;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.*;
import java.net.SocketException;

public class FtpServer {
    public static void main(String[] args) {
        FTPClient ftpClient = new FTPClient();
        try {
            connection(ftpClient);
            download(ftpClient);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void download(FTPClient ftpClient) {
        String remoteUrl = "public_html/";
        File downloadToDisk = new File("/home/zoheb/Pictures/ftp");
        try {
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadToDisk));
            Boolean result = ftpClient.retrieveFile(remoteUrl, outputStream);
            if (result) {
                System.out.println("File has been downlaoded successfuly");
            }
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.logout();
                    ftpClient.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private static void connection(FTPClient ftpClient) throws IOException {
        ftpClient.connect("mywebsite.org",21);
        ftpClient.user("MyUsername321");
        ftpClient.pass("");
        ftpClient.enterLocalPassiveMode();
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
    }


}
