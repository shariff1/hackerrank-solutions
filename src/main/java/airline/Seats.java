package airline;

public class Seats {

    public static void main(String[] args) {
        generateSeats();
    }

    private static void generateSeats() {
        int seat = 1;
        for (int i = 65; i <= 90; i++) {
            for (int j = 1; j <= 6; j++) {
                String result = (char) i + "-  " + seat;
                seat++;
                System.out.println(result);
            }
        }
    }
}

