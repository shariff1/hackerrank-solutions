package hacker;

public class ReverseString {
    public static void main(String[] args) {
        StringBuilder name = new StringBuilder("ahmed Shariff");
        name.reverse();
        System.out.println(name);
    }
}
