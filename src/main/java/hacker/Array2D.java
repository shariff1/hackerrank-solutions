package hacker;

import java.util.Scanner;

public class Array2D {
    public static void main(String[] args) {
        Scanner scanner
                 = new Scanner(System.in);
        int[][] grid= new int[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                grid[i][j] = scanner.nextInt();
            }
        }
        int number = Integer.MIN_VALUE;
        int temp;
        //[k =1] [k=2] [k=3] //i =1
        //     [k+1]      //i=2
        //[k =1] [k=2] [k=3] // i =3
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                temp = grid[i][j] + grid[i][j+1] + grid[i][j+2]
                        + grid[i+1][j+1]
                        + grid[i+2][j] + grid[i+2][j+1] + grid[i+2][j+2];
                number = Math.max(temp,number);

            }
        }
        System.out.println(number);


    }
}
