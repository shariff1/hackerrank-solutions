package hacker;

import java.util.ArrayList;
import java.util.Scanner;

public class DynamicArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int lastAns = 0;
        int sequence = scanner.nextInt();
        int query = scanner.nextInt();
        int x, y, index, type;
        ArrayList<Integer>[] seqList = new ArrayList[sequence];
        while (query-- > 0) {
            type = scanner.nextInt();
            x = scanner.nextInt();
            y = scanner.nextInt();
            index = (x ^ lastAns) % sequence;
            switch (type) {
                case 1:
                    if (seqList[index] == null) {
                        ArrayList<Integer> integerArrayList = new ArrayList<>();
                        integerArrayList.add(y);
                        seqList[index] = integerArrayList;
                    } else {
                        seqList[index].add(y);
                    }
                    break;
                case 2:
                    System.out.println(lastAns = seqList[index].get(y % seqList[index].size()));
                    break;
            }
        }
    }
}
