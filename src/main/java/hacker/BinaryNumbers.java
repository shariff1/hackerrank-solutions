package hacker;

import java.util.Scanner;

public class BinaryNumbers {
    public static void main(String[] args) {
        Scanner scanner
                = new Scanner(System.in);
        int number = scanner.nextInt();
        String[] result = Integer.toBinaryString(number).split("[0]+");
        int max =0;
        for (String s:result
             ) {
            if (s.length() > max){
                max = s.length();
            }

        }
        System.out.println(max);

    }
}
