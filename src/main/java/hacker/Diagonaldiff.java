package hacker;

import java.util.Scanner;

public class Diagonaldiff {
    public static void main(String[] args) {
        int pSum = 0, sSum = 0, N, input;
        Scanner in = new Scanner(System.in);
        N = in.nextInt();

        for(int i = 0; i < N; i ++){
            for(int j = 0; j < N ; j++){
                input = in.nextInt();
                if(i == j){
                    pSum += input;
                }
                if(j == N-(i+1)){
                    sSum += input;
                }
            }
        }
        System.out.println(Math.abs(pSum-sSum));
    }

}
/*
Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int a[][] = new int[n][n];
    int d1sum = 0;
    int d2sum = 0;
    for(int a_i=0; a_i < n; a_i++){
        for(int a_j=0; a_j < n; a_j++){
            a[a_i][a_j] = in.nextInt();
            if(a_j == a_i){
                d1sum += a[a_i][a_j];
            }
            if(a_i == n-a_j-1){
                d2sum += a[a_i][a_j];
            }

        }
    }

    int diff = d1sum-d2sum;
    System.out.println(Math.abs(diff));


}
 */