package hacker;

import java.util.Scanner;

public class Add {
    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);
        int firstValue = scanner.nextInt();
        int[] ar = new int[firstValue];
        for (int i = 0; i < firstValue; i++) {
            ar[i] = scanner.nextInt();
        }
        add(ar);

    }

    private static void add(int[] ar) {
        int sum = 0;
        String lineSeperator = "";
        for (int j : ar) {
            sum += j;
            System.out.print(lineSeperator + j);
            lineSeperator = "+";
        }
        System.out.println("=" + sum);
    }
}