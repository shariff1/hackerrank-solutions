package hacker;

import java.util.Scanner;

public class Fact {
    public static void main(String[] args) {
        Scanner scanner
                = new Scanner(System.in);
        int number = scanner.nextInt();
        int result = factorial(number);
        System.out.println("Factorial of "+number+" is: "+result);
    }

    private static int factorial(int number) {
        int fact = 1;
        for (int i = 1; i <= number; i++) {
            fact = fact * i;
        }

        return fact;
    }
}
