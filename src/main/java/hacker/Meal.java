package hacker;

import java.util.Scanner;

public class Meal {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double mealCost = scan.nextDouble(); // original meal price
        int tipPercent = scan.nextInt(); // tip percentage
        int taxPercent = scan.nextInt(); // tax percentage
        scan.close();

        // Write your calculation code here.
        int tip = calculateTip(tipPercent, mealCost);
        int tax = calculateTax(taxPercent, mealCost);


        // cast the result of the rounding operation to an int and save it as totalCost
        int totalCost = (int) Math.round(tip + tax + mealCost);
        System.out.println(totalCost);
        // Print your result
    }

    private static int calculateTax(int taxPercent, double mealCost) {
        return (int) Math.round(mealCost * taxPercent / 100);

    }

    private static int calculateTip(int tipPercent, double mealCost) {

        return (int) Math.round(mealCost * tipPercent / 100);
    }
}
