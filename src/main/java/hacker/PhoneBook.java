package hacker;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class PhoneBook {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberofEntries = scanner.nextInt();

        Map<String, Integer> phoneBook = new HashMap<>();
        for (int i = 0; i < numberofEntries; i++) {
            String name = scanner.next();
            int phone = scanner.nextInt();
            phoneBook.put(name, phone);
        }
        for (Map.Entry<String, Integer> entry : phoneBook.entrySet()
                ) {
            String searchName = scanner.next();
            if (phoneBook.containsKey(searchName)) {
                Integer fone = entry.getValue();
                System.out.println(searchName+ "="+ phoneBook.get(searchName));
            }else {
                System.out.println("Not found");
            }
        }

    }
}
