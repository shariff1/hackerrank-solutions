package hacker;

import java.util.Scanner;

public class Multiples {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        generateTables(number);
    }

    private static void generateTables(int number) {
        int count = 1;
        for (int i = 0; i < 10; i++) {
            System.out.println(number + "x" + count + "=" + number * count++);
        }
    }
}
