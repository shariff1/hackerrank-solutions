package hacker;

public interface Fizz {
    public void fizz(int a);

    public void buzz(int b);

    public void fizzbuzz(int c);

}
