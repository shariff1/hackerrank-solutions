package hacker;

import java.util.Scanner;

public class StringSpaces {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        for (int i = 0; i < number; i++) {
            String name = scanner.next();
            calculateNames(name);
        }
        scanner.close();

    }

    private static void calculateNames(String name) {
        StringBuffer odd = new StringBuffer();
        StringBuffer even = new StringBuffer();
        for (int i = 0; i < name.length(); i++) {
            char oddEven = name.charAt(i);
            if (i % 2 == 0) {
                even.append(oddEven);
            } else {
                odd.append(oddEven);
            }
        }
        System.out.println(even + " " + odd);
    }
}
