package hacker;

public class FizzBuzz implements Fizz {
    public static void main(String[] args) {
        calculateLogic();
    }

    private static void calculateLogic() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        for (int i = 1; i < 101; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                fizzBuzz.fizzbuzz(i);
            }
            else if (i % 3 == 0) {
                fizzBuzz.fizz(i);
            }
            else if (i % 5 == 0) {
                fizzBuzz.buzz(i);
            } else {
                System.out.println(i);
            }

        }
    }

    @Override
    public void fizz(int a) {
        System.out.println("fizz");
    }

    @Override
    public void buzz(int b) {
        System.out.println("bizz");
    }

    @Override
    public void fizzbuzz(int c) {
        System.out.println("fixbiz");
    }
}
